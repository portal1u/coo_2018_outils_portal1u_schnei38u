package animal;

public class Loup implements Animal {
	private int viande=0;
	private int pointDeVie;
	
	public Loup() {
		this.pointDeVie=30;
	}
	public Loup(int pv) {
		if(pv>=0)
			this.pointDeVie=pv;
		else
			this.pointDeVie=30;
	}

	
	public boolean etreMort() {
		return(this.pointDeVie==0);
	}

	
	public boolean passerUnjour() {
		boolean res=true;
		if(this.etreMort())
			res= false;
		else {
			if(this.viande>=1) {
				this.viande=this.viande-1;
				this.viande=this.viande/2;
			}
			else {
				if(this.pointDeVie-4<=0) {
					this.pointDeVie=0;
					res= false;
				}
				else if(this.pointDeVie-4>0) {
					this.pointDeVie=this.pointDeVie-4;
					this.viande=this.viande/2;
				}	
			}
			
		}
		return res;
		
	}

	
	public void stockerNourriture(int nourriture) {
		if (nourriture>0) {
			this.viande=this.viande+nourriture;
		}
	}

	
	public int getPv() {
		return this.pointDeVie;
	}

	public int getStockNourriture() {
		return this.viande;
	}
	public String toString() {
		return ("Loup - pv : "+this.pointDeVie+" viande : "+this.viande);
	}

}
