package animal;

public class Ecureuil implements Animal{

	
	int noisette;
	int pointDeVie;
	
	public Ecureuil () {
		 this.noisette=0;
		 this.pointDeVie=5;
	}
	
	public Ecureuil(int pv) {
		if (pv>=0)
			this.pointDeVie=pv;
		else 
			this.pointDeVie=5;
		this.noisette=0;
	}

	public boolean etreMort() {
		if (this.pointDeVie<=0)
			return true;
		else 
			return false;
	}


	public boolean passerUnjour() {
		boolean res=true;
		if(this.noisette>=1)
			this.noisette=this.noisette-1;
		else 
			this.pointDeVie-=2;
		if (this.noisette>=1)
			this.noisette-=1;
		
		if (etreMort())
			this.pointDeVie=0;
			res = false;
		
		return res;
	}


	public void stockerNourriture(int nourriture) {
		if (nourriture>0) {
			this.noisette=this.noisette+nourriture;
		}
	}


	public int getPv() {
		return this.pointDeVie;
	}


	public int getStockNourriture() {
		return this.noisette;
	}
	
	public String toString() {
		return "Ecureil - pv: " + this.pointDeVie + "noisettes: " + this.noisette;
	}

}
