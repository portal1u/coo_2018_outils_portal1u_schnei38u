package test;
import animal.Ecureuil;
import animal.Loup;
import static org.junit.Assert.*;

import org.junit.Test;

public class TestLoup {
	/* 
	 * test du constructeur sans paramètre
	 */
	@Test
	public void testConstructeur() {
		Loup l1 = new Loup();

		
		assertEquals("le nombre de points de vie devrait etre 30 ",30,l1.getPv());
		assertEquals("le nombre de viande devrait etre 0",0,l1.getStockNourriture());
	}
	
	
	/* 
	 * test du constructeur avec paramètre positif
	 */
	@Test
	public void testConstructeur1() {
		Loup l = new Loup(10);
		assertEquals("le nombre de points de vie devrait etre 10 ",10,l.getPv());
		assertEquals("le nombre de viande devrait etre 0",0,l.getStockNourriture());

	}
	
	/* 
	 * test du constructeur avec paramètre negatif
	 */
	@Test
	public void testConstructeur2() {
		Loup l = new Loup(-10);
		assertEquals("le nombre de points de vie devrait etre 30 ",30,l.getPv());
		assertEquals("le nombre de viande devrait etre 0",0,l.getStockNourriture());
	}
	
	/* 
	 * test du constructeur avec  0 en paramètre 
	 */
	@Test
	public void testConstructeur3() {
		Loup l = new Loup(0);
		assertEquals("le nombre de points de vie devrait etre 0 ",0,l.getPv());
		assertEquals("le nombre de viande devrait etre 0",0,l.getStockNourriture());
	}
	
	/* 
	 * test de la methode stockerNouriture avec un paramètre positif
	 */
	@Test
	public void teststockerNourritureNormal() {
		Loup l = new Loup();
		l.stockerNourriture(3);
		assertEquals("le nombre de viande devrait etre 3",3,l.getStockNourriture());
	}
	
	/* 
	 * test de la methode stockerNouriture avec 0 en paramètre
	 */
	@Test
	public void teststockerNourritureNull() {
		Loup l = new Loup();
		l.stockerNourriture(0);
		assertEquals("le nombre de viande devrait etre 0",0,l.getStockNourriture());
	}
	
	/* 
	 * test de la methode stockerNouriture avec un paramètre negatif
	 */
	@Test
	public void teststockerNourritureNegatif() {
		Loup l = new Loup();
		l.stockerNourriture(-5);
		assertEquals("le nombre de viande devrait etre 0",0,l.getStockNourriture());
	}
	
	@Test
	public void testPasserUnJour1 (){
		Loup l =new Loup ();
		l.stockerNourriture(10);
		l.passerUnjour();
		assertEquals ("le loup devrait etre vivant" ,false,l.etreMort());
		assertEquals ("le loup avoir 5 pv" ,30,l.getPv());
		assertEquals ("les viandes devraient etre à 4" ,4, l.getStockNourriture());
		}
		@Test
		/* test passer un jour
		 * a pas assez de nourriture
		 * a assez de point de vie
		 */
		public void testPasserUnJour2 (){
		Loup l =new Loup ();
		l.passerUnjour();
		assertEquals ("le loup devrait etre vivant" ,false,l.etreMort());
		assertEquals ("le loup avoir 26 pv" ,26,l.getPv());
		assertEquals ("les viandes devraient etre à 0" ,0, l.getStockNourriture());
		}
		@Test
		/* test passer un jour
		 * a pas assez de nourriture
		 * a pas assez de point de vie
		 */
		public void testPasserUnJour3 (){
		Loup l =new Loup (1);
		l.passerUnjour();
		assertEquals ("le loup devrait etre mort" ,true,l.etreMort());
		assertEquals ("le loup devrait avoir 0 pv" ,0,l.getPv());
		assertEquals ("les viandes devraient etre à 0" ,0, l.getStockNourriture());
		}
}
