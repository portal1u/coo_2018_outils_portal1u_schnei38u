package test;
import animal.Ecureuil;
import static org.junit.Assert.*;

import org.junit.Test;

public class TestEcureuil {

	@Test
	/* test du contructeur par default
	 */
	public void testConstructeurDefault (){
	Ecureuil e =new Ecureuil ();
	assertEquals ("la vie devrait etre 5 ",5 , e.getPv() );
	assertEquals ("les noisettes devraient etre � 0" ,0 , e.getStockNourriture());
	}
	
	@Test
	/* test du contructeur cas ok
	 */
	public void testConstructeurOk (){
	Ecureuil e =new Ecureuil (5);
	assertEquals ("la vie devrait etre 5 ",5 , e.getPv() );
	assertEquals ("les noisettes devraient etre � 0" ,0 , e.getStockNourriture());
	}
	@Test
	/* test du contructeur cas negatif
	 */
	public void testConstructeurNegatif (){
	Ecureuil e =new Ecureuil (-5);
	assertEquals ("la vie devrait etre 5 ",5 , e.getPv() );
	assertEquals ("les noisettes devraient etre � 0" ,0 , e.getStockNourriture());
	}
	@Test
	/* test du contructeur cas null
	 */
	public void testConstructeurNull (){
	Ecureuil e =new Ecureuil (0);
	assertEquals ("la vie devrait etre 0 ",0 , e.getPv() );
	assertEquals ("les noisettes devraient etre � 0" ,0 , e.getStockNourriture());
	}
	@Test
	/* test stockernourriture positif
	 */
	public void testStockerNourriturePositif (){
	Ecureuil e =new Ecureuil ();
	e.stockerNourriture(2);
	assertEquals ("les noisettes devraient etre � 2" ,2, e.getStockNourriture());
	}
	@Test
	/* test stockernourriture negatif
	 */
	public void testStockerNourritureNegatif (){
	Ecureuil e =new Ecureuil ();
	e.stockerNourriture(2);
	e.stockerNourriture(-2);
	assertEquals ("les noisettes devraient etre � 2" ,2, e.getStockNourriture());
	}
	@Test
	/* test stockernourriture null
	 */
	public void testStockerNourritureNull (){
	Ecureuil e =new Ecureuil ();
	e.stockerNourriture(0);
	assertEquals ("les noisettes devraient etre � 0" ,0, e.getStockNourriture());
	}
	@Test
	/* test passer un jour
	 * a assez de nourriture
	 */
	public void testPasserUnJour1 (){
	Ecureuil e =new Ecureuil ();
	e.stockerNourriture(4);
	e.passerUnjour();
	assertEquals ("l'ecureuil devrait etre vivant" ,false,e.etreMort());
	assertEquals ("l'ecureuil devrait avoir 5 pv" ,5,e.getPv());
	assertEquals ("l'ecureuil devrait avoir  2 noisettes" ,2, e.getStockNourriture());
	}
	@Test
	/* test passer un jour
	 * a juste assez de nourriture
	 */
	public void testPasserUnJour2 (){
	Ecureuil e =new Ecureuil (8);
	e.stockerNourriture(1);
	e.passerUnjour();
	assertEquals ("l'ecureuil devrait etre vivant" ,false,e.etreMort());
	assertEquals ("l'ecureuil devrait avoir 8 pv" ,8,e.getPv());
	assertEquals ("les noisettes devraient etre � 0" ,0, e.getStockNourriture());
	}
	@Test
	/* test passer un jour
	 * a pas assez de nourriture
	 * a assez de point de vie
	 * avait pas de noisettes
	 */
	public void testPasserUnJour3 (){
	Ecureuil e =new Ecureuil (8);
	e.passerUnjour();
	
	assertEquals ("l'ecureuil devrait etre vivant" ,false,e.etreMort());
	assertEquals ("l'ecureuil devrait avoir 6 pv" ,6,e.getPv());
	assertEquals ("les noisettes devraient etre � 0" ,0, e.getStockNourriture());
	}
	@Test
	/* test passer un jour
	 * a pas assez de nourriture
	 * a pas assez de point de vie
	 */
	public void testPasserUnJour4 (){
	Ecureuil e =new Ecureuil (1);
	e.passerUnjour();
	assertEquals ("l'ecureuil devrait etre mort" ,true,e.etreMort());
	assertEquals ("l'ecureuil avoir 0 pv" ,0,e.getPv());
	assertEquals ("les noisettes devraient etre � 0" ,0, e.getStockNourriture());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
